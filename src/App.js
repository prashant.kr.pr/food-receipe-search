import React, { Component } from 'react';
import './App.css';
import RecipeList from './components/RecipeList';
import RecipeDetails from './components/RecipeDetails';
import { recipes } from './tempList';

class App extends Component {
  state = {
    recipe: recipes,
    url: "https://www.food2fork.com/api/search?key=2b5d989df642cc591085f29fb5bd54f8",
    base_url: "https://www.food2fork.com/api/search?key=2b5d989df642cc591085f29fb5bd54f8",
    details_id: 47746,
    pageIndex: 1,
    search: '',
    query: "&q=",
    error: ''
  }
  // ajax handling
  async getRecipes() {
    try {


      const data = await fetch(this.state.url);
      const jsonData = await data.json()
      if (jsonData.recipes.length === 0) {
        this.setState({
          error: 'sorry, No recipe found for the searched term.'
        })
      } else {
        this.setState({
          recipe: jsonData.recipes
        })
      }

    } catch (error) {
      console.log(error);
    }
  }
  componentDidMount() {
    this.getRecipes();
  }

  displayPage = (index) => {
    switch (index) {
      default:
      case 1:
        return (<RecipeList handleDetails={this.handleDetails}
          recipes={this.state.recipe}
          value={this.state.search}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          error={this.state.error}
        />)
      case 0:
        return (<RecipeDetails handleIndex={this.handleIndex} id={this.state.details_id} />)
    }
  }
  handleIndex = index => {
    this.setState({
      pageIndex: index
    });
  };
  handleDetails = (index, id) => {
    this.setState({
      pageIndex: index,
      details_id: id
    });
  };

  handleChange = e => {
    this.setState({
      search: e.target.value
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    const { base_url, query, search } = this.state;
    this.setState(() => {
      return { url: `${base_url}${query}${search}`, search: "" }
    }, () => {
      this.getRecipes();
    })
  }

  render() {
    return (
      <React.Fragment>
        {
          this.displayPage(this.state.pageIndex)
        }

      </React.Fragment>
    );
  }
}

export default App;
